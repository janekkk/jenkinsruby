pipeline {
    agent any

    parameters {
        choice(choices: ['master', 'develop', 'team_habs', 'team_a', 'team_32', 'team_fido', 'team_clockwork', 'team_lava'],
                    description: 'The branch from which the deployment data is downloaded',
                    name: 'branch'
        )
        string(defaultValue: '',
               description: 'The web services version from which licenses are downloaded (leave empty if you want the latest version)',
               name: 'wsversion',
        )
        string(defaultValue: '',
               description: 'The platform from which web services version is taken (required if wsversion is empty)',
               name: 'platform',
        )
        string(defaultValue: '',
               description: 'The environment from which web services version is taken (required if wsversion is empty)',
               name: 'environment',
        )
    }

    triggers {
            parameterizedCron('''
                0 15 * * 1-5 %branch=dev;wsversion=;platform=dev;environment=dev
            ''')
    }

    stages {
        stage('Checking the latest version of WebServices') {
            when {
                expression {wsversion.isEmpty()}
            }
            steps {
                script {
                    echo "wsversion parameter empty -- taking latest version (platform: ${platform}, environment: ${environment})"
                    wsversion  = sh (script: "C:/Ruby31-x64/bin/ruby.exe hello.rb -u https://version-management-development-tools.apps.core.cias.one -p ${platform} -e ${environment}", returnStdout: true)
                    .trim()
                }
            }
        }

        stage('Running 3rd party license tool') {
            steps {
                script {
                    echo "Selected wsversion=${wsversion}, branch=${branch}"
                    sh "mkdir out"
                    sh "touch out/xd.txt; echo \"xddddd\" > out/xd.txt"
                }
            }
        }
        stage('Push to nexus') {
            steps {
                   sh "mkdir zip && cp out zip/ || exit 0"
                   zip zipFile: "licenses-${wsversion}-${branch}-${env.BUILD_NUMBER}.zip", dir: 'zip'
                   archiveArtifacts artifacts: "licenses-${wsversion}-${branch}-${env.BUILD_NUMBER}.zip", fingerprint: true
            }
        }
    }

    post {
        failure {
            slackSend channel: "#descr", message: "License report job failed. Check logs at ${env.BUILD_URL}"
        }
    }
}
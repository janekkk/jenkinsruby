#! /usr/bin/env ruby

require 'uri'
require 'net/http'
require 'json'
require 'logger'
require 'optparse'

$logger = Logger.new(STDERR)

args = {}
OptionParser.new do |opts|
  opts.on('-u [ARG]', '--uri [ARG]', "Specify the versioning base uri") do |v|
    args[:uri] = v
  end
  opts.on('-p [ARG]', '--platform [ARG]', "Specify the platform") do |v|
    args[:platform] = v
  end
  opts.on('-e [ARG]', '--environment [ARG]', "Specify the environment") do |v|
    args[:environment] = v
  end
end.parse!

def get_current_version
return '1.2.5'
end

puts get_current_version